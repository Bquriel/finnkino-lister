package finnkino;

import java.util.ArrayList;

public class Cinema {
    private ArrayList<SingleCinema> cinemas;
    
    public Cinema(){
        cinemas = new ArrayList<>();
    }
    
    public void addCinema( String loc, String id ){
        cinemas.add( new SingleCinema( loc, id ));
    }
    
    public ArrayList<SingleCinema> getSingleCinemas(){ return cinemas; }
}


class SingleCinema{
    private final String location;
    private final String id;

    public SingleCinema( String loc, String id ){
        this.location = loc;
        this.id = id;
    }
    
    @Override
    public String toString(){ return location; }
    public String getId()   { return id; }
    public String getName() { return location; }
}