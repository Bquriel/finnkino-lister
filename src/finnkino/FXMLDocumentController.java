package finnkino;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

public class FXMLDocumentController implements Initializable {
    
    Cinema cinema;
    XMLLoader loader;
    @FXML
    private ComboBox<SingleCinema> theatreBox;
    @FXML
    private TextField dateLabel;
    @FXML
    private ListView<String> listView;
    @FXML
    private TextField startTime;
    @FXML
    private TextField endTime;
    @FXML
    private TextField filmName;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cinema = new Cinema();
        loader = new XMLLoader();
        loader.loadCinemaInfo( cinema );
        for( SingleCinema cin : cinema.getSingleCinemas() ){
            theatreBox.getItems().add( cin );
        }
            theatreBox.setValue( theatreBox.getItems().get( 0 ));
    }    

    @FXML
    private void onListFilmsPressed(ActionEvent event) {
        listView.getItems().clear();
        String id = theatreBox.getSelectionModel().getSelectedItem().getId();
        String start = getStartTime();
        String end = getEndTime();
        ArrayList<String> shows = loader.loadShowInfo( getDate(), id );
        listView.getItems().add( theatreBox.getSelectionModel().getSelectedItem().getName() + ":" );
        for( String line : shows ){
            String[] temp = line.split( ";" );
            Float time = Float.valueOf( temp[1] );
            if( time >= Float.valueOf( start ) && time <= Float.valueOf( end )){
                listView.getItems().add( "\t" + temp[0] + "\t" + temp[1] );
            }
        }
    }

    @FXML
    private void onSeaechByNamePressed(ActionEvent event) {
        listView.getItems().clear();
        String name = filmName.getText();
        for( SingleCinema cin : cinema.getSingleCinemas()){
            listView.getItems().add( cin.getName() + ":" );
            ArrayList<String> shows = loader.loadShowInfoByName( name, getDate(), cin);
            for( String line : shows ){
                String[] temp = line.split( ";" );
                if( temp[0].contains( name )){
                    listView.getItems().add( "\t" + temp[0] + "\t" + temp[1] );
                }
            }
        }
        
    }
    
    private String getDate(){
        String date = dateLabel.getText();
        if( date.isEmpty()){
            date = new SimpleDateFormat( "dd.MM.yyyy" ).format( new Date());
        }        
        return date;
    }
    
    private String getStartTime(){
        String start = startTime.getText();
        if( start.isEmpty()){
            start = "00.00";
        }
        return start;
    }
    
    private String getEndTime(){
        String end = endTime.getText();
        if( end.isEmpty()){
            end = "23.59";
        }        
        return end;
    }
}
/*
timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
SimpleDateFormat toDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
Date beginDate = toDate.parse(beginDateStr);
Date endDate = toDate.parse(endDateStr);
*/