package finnkino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLLoader {
    
    public XMLLoader(){
    }
    
    public void loadCinemaInfo( Cinema cinema ){
        String text = loadData( "http://www.finnkino.fi/xml/TheatreAreas/" );
        Document doc = prepDoc( text );
        parseCinemas( cinema, doc );
    }
    
    public ArrayList<String> loadShowInfo( String date, String id ){
        String adress = "http://www.finnkino.fi/xml/Schedule/?area=" + id +
                "&dt=" + date;
        String text = loadData( adress );
        Document doc = prepDoc( text );
        return parseShows( doc );
    }
    
    public ArrayList<String> loadShowInfoByName( String name, String date, SingleCinema cinema ){
        String id = cinema.getId();
        String adress = "http://www.finnkino.fi/xml/Schedule/?area=" + id +
                "&dt=" + date; 
        String text = loadData( adress );
        Document doc = prepDoc( text );
        return parseShows( doc );
    }
    
    private String loadData( String adress ){
        String text = "";
        try{
            URL url = new URL( adress );
            BufferedReader reader = new BufferedReader( new InputStreamReader( url.openStream()));
            String line;
            while(( line = reader.readLine()) != null ){
                text += line + "\n";
            }
        } catch( Exception ex ){
            System.out.println("Virhe XML:n lataamisessa!");
        }
        return text;
    }
    
    private Document prepDoc( String text ){
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = null;
        try {
            DocumentBuilder builder = dbf.newDocumentBuilder();
            doc = builder.parse( new InputSource( new StringReader( text )));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(XMLLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return doc;
    }
    
    private void parseCinemas( Cinema cinema, Document doc ){
        NodeList nodes = doc.getElementsByTagName( "TheatreArea" );
        String location;
        String id;
        for( int i = 0; i < nodes.getLength(); i++ ){
            Node node = nodes.item( i );
            Element element = (Element) node;
            location = element.getElementsByTagName("Name").item(0).getTextContent();
            id = element.getElementsByTagName("ID").item(0).getTextContent();
            cinema.addCinema( location , id );
        }    
    }
    
    private ArrayList<String> parseShows( Document doc ){
        ArrayList<String> text = new ArrayList();
        NodeList nodes = doc.getElementsByTagName( "Show" );
        for( int i = 0; i < nodes.getLength(); i++ ){
            Node node = nodes.item( i );
            Element element = (Element) node;
            String line = element.getElementsByTagName("Title").item(0).getTextContent();
            String temp1 = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();
            String[] temp2 = temp1.split( "T" );
            temp2 = temp2[1].split( ":" );
            text.add( line + ";" + temp2[0] + "." + temp2[1] );
        }
        return text;
    }
    
}